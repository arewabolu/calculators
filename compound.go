package main

import (
	"flag"
	"fmt"
	"math"
)

// Principal value
var (
	Pval   float64
	tval   int
	perval float64
)

const rebase int = 3 // rebase every 8 hours i.e 3rebases per day

type initVal struct {
	Principal float64 //principal
	Time      int     //duration in days
	Interest  float64 //percentage of interest
}

//a function to calculate compound interests
func (values *initVal) compoundCalc() float64 {

	k := float64(rebase) * float64(values.Time)
	//
	baseVal := 1 + (values.Interest / 100)
	//totalValue := math.Pow(compVal, k)
	return values.Principal * (math.Pow(baseVal, k))
}

func main() {
	flag.Float64Var(&Pval, "P", 1.0, "Principal Value")
	flag.IntVar(&tval, "T", 1, "time duration for investment in days")
	flag.Float64Var(&perval, "I", 0.0, "Percentage Interest")

	flag.Parse()

	compVal := initVal{Pval, tval, perval}
	fmt.Println(compVal)

	fmt.Println("The compound value is:", compVal.compoundCalc())

}
