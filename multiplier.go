package main

import (
	"flag"
	"fmt"
	"os"
)

var newFlag flag.Flag

type Temp struct {
	celius     float64
	farehnheit float64
	kelvin     float64
}

// Program to convert from Fahrenheit to Celsius
func (temp *Temp) CtoFConv() float64 {

	F := temp.celius - 32*5/9
	return F
}

/*func converter() {
	fmt.Print("Enter a number: ")
	var FT float64
	fmt.Scanf("%f", &FT)
	M := FT * 0.3048
	fmt.Println(M)
}*/

func main() {

	C := flag.Float64("c", 0, "Celsius value to be converted")
	F := flag.Float64("f", 0, "Celsius value to be converted")
	K := flag.Float64("k", 0, "Celsius value to be converter")

	flag.Parse()

	celVal := Temp{
		celius:     *C,
		farehnheit: *F,
		kelvin:     *K,
	}

	switch os.Args[1] {
	case "c":
		fmt.Println(celVal.CtoFConv())

	}

}
